# Package

version       = "0.1.0"
author        = "Yuli Che."
description   = "VT widget kit, for producing terminal ui applications"
license       = "MIT"
srcDir        = "src"
installExt    = @["nim"]
bin           = @["menu", "ask", "hr"]


# Dependencies

requires "nim >= 1.4.4"
