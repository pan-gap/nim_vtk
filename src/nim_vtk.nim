import terminal

const
    go_up*     = "\e[A"
    go_down*   = "\e[B"
    clr_ln*    = "\e[2K"
    clr_left*  = "\e[0K"
    clr_right* = "\e[1K"
    cur_home*  = "\e[0G"
    ins_ln*    = "\e[1L"
    del_ln*    = "\e[1M"

    reset_all* = "\e[m"
    reset_mode* ="\e[0m"
    reset_fg*  = "\e[39m"
    reset_bg*  = "\e[49m"
    no_bold*   = "\e[22m"
    no_faint*  = "\e[22m"
    no_italic* = "\e[23m"
    no_under*  = "\e[24m"
    no_blink*  = "\e[25m"
    no_rev*    = "\e[27m"
    no_inv*    = "\e[28m"
    no_strike* = "\e[29m"

    bold*      = "\e[1m"
    faint*     = "\e[2m"
    italic*    = "\e[3m"
    under*     = "\e[4m"
    blink*     = "\e[5m"
    rev*       = "\e[7m"
    inv*       = "\e[8m"
    strike*    = "\e[9m"

proc rep_char* (char:string, times:Natural) :string =
  if 1 < times:
    return char & "\e[" & $(times - 1) & "b"
  if 0 == times:
    return ""
  return char

proc cur_hide* =
  stderr.write "\e[?25l"

proc cur_show* =
  stderr.write "\e[?25h"

proc cur_col* :string =
  stderr.write "\e[6n"
  var
    s= ""
    c= getch()
  
  while ';' != c:
    c= getch()
  c= getch()
  
  while '1' == c or '2' == c or '3' == c or '4' == c or '5' == c or '6' == c or '7' == c or '8' == c or '9' == c or '0' == c:
    s.add c
    c= getch()
  
  return s

proc cur_ln* :string =
  stderr.write "\e[6n"
  var
    s= ""
    c= getch()
  
  while '1' != c and '2' != c and '3' != c and '4' != c and '5' != c and '6' != c and '7' != c and '8' != c and '9' != c and '0' != c:
    c= getch()
  
  while '1' == c or '2' == c or '3' == c or '4' == c or '5' == c or '6' == c or '7' == c or '8' == c or '9' == c or '0' == c:
    s.add c
    c= getch()
  
  return s

