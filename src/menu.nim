import os, terminal
import strscans, strutils
import nim_vtk


when isMainModule:
  # var cols= terminalWidth()
  let item= commandLineParams()
  
  if 0 == item.len:
    system.quit()

  case item[0]:
    of "-h", "--help":
      echo r"Usage:  menu [OPTIONS] ITEM1 ITEM2 ..."
      echo r"Returns selected item number, or 0 if Ctrl-C."
      echo r"Arrow keys to move, Enter/Space to select."  # FIXME Esc key cancels
      echo "Works well for 2 to 20 items.\n"
  #    echo r"-s       supress animation"
      echo r"-c NUM   use color 1-7"
      echo r"-i NUM   pre-select ITEM, defaults to 1"
      echo "-x       echo resulting ITEM back instead of return code\n"
      system.quit()
  
  var
    menu:seq[string]
  #  speed= false
    index= 0
    delim= "\n"
    color= 7
    do_print= false
    done_parsing_args= true
  
  for i, x in item:
    case x:
  #    of "-s", "--speed":
  #      speed= true
  #      done_parsing_args= true
      of "-i", "--item":
        discard item[ i+1].scanf("$i", index)
        index = index - 1
        done_parsing_args= false
      of "-x", "--echo":
        do_print= true
        done_parsing_args= true
      of "-c", "--color":
        discard item[ i+1].scanf("$i", color)
        done_parsing_args= false
      else:
        if done_parsing_args:
          menu.add x
        done_parsing_args= true
  
  var
    result=  ""
    is_last= menu.len - 1
    bg=      if 7 != color: 7 else: 0
    line=    rev & "\e[3" & $color & ";4" & $bg & "m"
    go_back= "\e[" & $is_last & "A" & cur_home
    reset_line= reset_mode
    padding= " "
    did_break= false
  
  
  for i, x in menu:
    if index == i:
      result.add line & padding & x & padding & reset_line
    else:
      result.add padding & x & padding
    if i < is_last:
      result.add delim
  
  cur_hide()
  stderr.write result & go_back
  
  var got= $getch()
  if "[" == got:
    got= $getch()
    if got in ["A", "D"]:
      index= max( 0, index - 1)
    if got in ["B", "C"]:
      index= min( is_last, index + 1)
  elif 3 == ord(got[0]):  #FIXME add handling for Esc key
    did_break= true
  
  while not did_break and not (got in [" ", "\r"]):
    #FIXME can this be both more CPU-friendly, but also not too code-smelly?
    result= ""
    for i, x in menu:
      if index == i:
        result.add line & padding & x & padding & reset_line
      else:
        result.add padding & x & padding
      if i < is_last:
        result.add delim
    
    stderr.write result & go_back
    got= $getch()
    if "[" == got:
      got= $getch()
      if got == "A":
        index= max( 0, index - 1)
      if got == "B":
        index= min( is_last, index + 1)
      if got == "C":
        index= is_last
      if got == "D":
        index= 0
    elif 3 == ord(got[0]):  #FIXME add handling for Esc key
      did_break= true
  
  stderr.write (clr_ln & go_down).repeat( is_last + 1) & go_up.repeat( is_last + 1)
  
  cur_show()
  if not did_break:
    if do_print:
      echo $menu[ index]
      system.quit 1 + index
    else:
      system.quit 1 + index


