import os, terminal
import strscans, strutils
import nim_vtk


let
  item= commandLineParams()
  cols= terminalWidth()

if 0 < item.len:
  case item[0]:
    of "-h", "--help":
      echo r"Usage:  hr [-c COLOR 0-7] [CHAR]"
      echo "Fill a line with --- marking it in your terminal.\n"
      system.quit()

var
  bullet= "-"
  color= 7
  done_parsing_args= false

for i, x in item:
  case x:
    of "-c", "--color":
      discard item[ i+1].scanf("$i", color)
    else:
      if done_parsing_args:
        bullet= x
      else:
        done_parsing_args= true

let
  line=    "\e[2;3" & $color & "m"
  stop=    "\t" & bullet & bullet & bullet
  #stop=    " ".rep_char(5) & bullet.rep_char(3)

# this can both take a background, and be > to stderr this way
stdout.write line &  stop.repeat( cols div 8 - 1)  & reset_all & "\n"


