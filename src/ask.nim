import os, terminal
import strscans
import nim_vtk


let item= commandLineParams()

if 0 < item.len:
  case item[0]:
    of "-h", "--help":
      echo r"Usage:  ask [OPTIONS] PROMPT ITEM1 ITEM2 ..."
      echo r"Ask a question, get an answer or 0."
      echo r"Arrow keys to move, Enter or Space to choose, Ctrl-C to cancel."  # FIXME Esc key cancels
      echo "Works well for 2 to 10 items.\n"
  #    echo r"-s       supress animation"
      echo r"-c NUM   use color 1-7"
      echo r"-i NUM   pre-select ITEM, defaults to 1"
      echo r"-d STR   menu item delimiter, defaults to SPACE"
      echo "-x       echo resulting ITEM back instead of return code\n"
      system.quit()

var
  menu:seq[string]
#  speed= false
  index= 0
  prompt:string
  color= 7
  do_print= false
  done_parsing_args= true
  delim= " "

for i, x in item:
  case x:
#    of "-s", "--speed":
#      speed= true
#      done_parsing_args= true
    of "-i", "--item":
      discard item[ i+1].scanf("$i", index)
      index = index - 1
      done_parsing_args= false
    of "-d", "--delim":
      delim= item[ i+1]
      done_parsing_args= false
    of "-x", "--echo":
      do_print= true
      done_parsing_args= true
    of "-c", "--color":
      discard item[ i+1].scanf("$i", color)
      done_parsing_args= false
    else:
      if done_parsing_args:
        if 0 != prompt.len:
          menu.add x
        else:
          prompt= x & reset_all
      done_parsing_args= true

var
  result=  ""
  did_break= false
let
  is_last= menu.len - 1
  bg=      if 7 != color: 7 else: 0
  line=    rev & "\e[3" & $color & ";4" & $bg & "m"
  reset_line= "\e[0m"
  padding= " "

for i, x in menu:
  if index == i:
    result.add line & padding & x & padding & reset_line
  else:
    result.add padding & x & padding
  if i < is_last:
    result.add delim

cur_hide()
stderr.write prompt
let
  start=   cur_col()
  go_back= "\e[" & start & "G"
stderr.write result & go_back

var got= $getch()
if "[" == got:
  got= $getch()
  if got in ["A", "D"]:
    index= max( 0, index - 1)
  if got in ["B", "C"]:
    index= min( is_last, index + 1)
elif 3 == ord(got[0]):  #FIXME add handling for Esc key
  did_break= true

while not did_break and not (got in [" ", "\r"]):
  #FIXME can this be both more CPU-friendly, but also not too code-smelly?
  result= ""
  for i, x in menu:
    if index == i:
      result.add line & padding & x & padding & reset_line
    else:
      result.add padding & x & padding
    if i < is_last:
      result.add delim
  
  stderr.write result & go_back
  got= $getch()
  if "[" == got:
    got= $getch()
    if got == "D":
      index= max( 0, index - 1)
    if got == "C":
      index= min( is_last, index + 1)
    if got == "A":
      index= 0
    if got == "B":
      index= is_last
  elif 3 == ord(got[0]):  #FIXME add handling for Esc key
    did_break= true

if do_print:
  stderr.write cur_home & "\e[" & start & "C" & clr_left
else:
  stderr.write cur_home & clr_ln

cur_show()
if not did_break:
  if do_print:
    echo $menu[ index]
    system.quit 1 + index
  else:
    system.quit 1 + index


