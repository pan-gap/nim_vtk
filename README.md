Terminal emulators display a grid of cells with scrollback, and an alternate
screen sized to the viewport. By sending character bytes to the terminal the
grid is filled with text. Sending and receiving specially formatted byte code
from the terminal allows for moving the cursor, deleting text, colors, arrow keys
and mouse actions.

FIXME asciinema

This is like a simpler, clunkier DOM for you front-end guys.

![](htdocs/logo-86.gif)  
** ANSI colors and simple widgets for terminal emulators **

Nim-VTK is an abstraction, a library for interacting with terminal emulators
from your Nim code. It can manipulate terminal contents for you and has
related utilities like a run-loop and some common unixy widgets for your app.

It is used to build dashboard-like apps with interactive gauges and meters.
Similarly, a project management tool uses this library.

FIXME asciinema

The simple scripting tools that come with Nim-VTK can be used to automate
interactive build and templating systems with question/answers and prompts.
Those tools are very robust in automating simple tasks in live terminal
situations.

# Download

## Binary utilities for use in Bash/Fish scripting

Below tools are useful both on their own, in scripting and interaction automation; and as examples of interfacing with the library and terminal emulators in general.

### `menu`, `ask` - Pick from a list

The difference is that `menu` is vertical, and `ask` will take a message first and is
horizontal, staying on the same line.

FIXME asciinema

* * *

# ROADMAP

<small>make what is needed, abstract out common tools and parts</small>

- [ ] terminal renderer, extend with bytecode-SVG for graphics
- [ ] terminal multiplexer, splits, mouse resize, zoom, force kill pane
- [ ] terminal emulator, to be extended
- [ ] wrap all components for library use
- [ ] fzf-like - Search/pick from a list
- [ ] notational-velocity-like - Add new or search/pick from a list
- [ ] color picker, with arrow keys or mouse HSV into HTML/ANSI colors
- [ ] line editor, to accompany `ask` with `read-tpl`
- [ ] ls-tree-ag-like - for file picking
- [ ] Not hanging randomly on some `getch()` calls

# CHANGELOG

- [x] `hr` for indicating truncated output, or vertical spacing

** 2021-32 Initial release **

- [x] handy quick color, and custom spacer support
- [x] `menu -i 1 N1 N2 N3` for default option in pre-defined menu
- [x] `ask -x Q ls ll cd |sh` for returning the text of the option

### License MIT
# WHY DO THIS

> As a user of similar tools I am generally dissatisfied with them on several
> levels of analysis. The UI is not always even close to what I'd want for
> myself, and the interaction speed of these fantastic, very robust, configurable
> and portable TUI things leaves a lot to be desired.
> 
> To my surprise the vocabulary I had on approach to this ANSI terminal space
> was both insufficient and inadequate. We're all learning, is what I say.
> 
> I had to learn about terminals, terminal emulators, Nim and **Lua**. Those two
> compete in portability/speed but in usability there is no competition. I mean
> they are both great. But I think I might have broken my brain's Python.
> 
> **Nim** and Lua were arrived at by fiddling with code in Fish, Python, D, and Zig.
> Not Javascript, because definitely slow.
> 
> Fish is not very extendable, Python was kinda on the slow side, D is either
> a closely guarded secret or a horse too tall to saddle, and Zig doesn't run on
> my 2010 Mac which I am perfectly happy with, thank you very much.
> 
> So the tools were chosen for their fitness to the purpose.

So, in short I make this so that I can have it and use it.

I hope you found here something that helps you out, or even just learned
something new yourself. If so, you should feel the appropriate level of smug
self-satisfaction.  
High-five.

You can [reach out on Twitter](https://ctt.ac/fq52_) for a quick question.


